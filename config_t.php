<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mariadb';
$CFG->dblibrary = 'native';
$CFG->dbhost    = '172.19.0.2';
$CFG->dbname    = 'bitnami_moodle';
$CFG->dbuser    = 'bn_moodle';
$CFG->dbpass    = '';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 3306,
  'dbsocket' => '',
  'dbcollation' => 'utf8_general_ci',
);

if (empty($_SERVER['HTTP_HOST'])) {
 $_SERVER['HTTP_HOST'] = 'localhost:80';
    }

    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
 $CFG->wwwroot   = 'https://' . $_SERVER['HTTP_HOST'];
    } else {
 $CFG->wwwroot   = 'http://' . $_SERVER['HTTP_HOST'];
    };
$CFG->dataroot  = '/data/moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 02775;

require_once($__DIR__. '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
